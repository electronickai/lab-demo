# Gitlab Meetup

## Installation and introduction of Lab

Please refer to the Lab project to get the latest information:  
https://github.com/zaquestion/lab

## Overview of Lab possibilities (v 0.16.0)

### What works quite well (and what we will do in some minutes)
- create projects
- list projects
- clone projects (quite convinient - and also for projects belonging to a group)
- fork project (probably not shown today)
- create and edit issues (title, description, assignee, labels and comments)
- work with merge requests
- lint pipeline .gitlab-ci.yml
- have a look into the ci status and logs
- Pass variables to a pipeline (probably not shown today)

### What isn't supported yet
- Create and maintain users and groups
- Create and maintain milestones
- Delete projects
- Fork projects that are on a different host than configured with Lab
- Add following issue fields
  - Milestone, Due Date and all the other stuff exept of title, message, assignee, labels and comments
- Check the comments of an issue (but you can click on the gitlab-url for details)
- Revoke a thumb up / thumb down
- Common administrative tasks like
    - Managing gitlab runners for a pipeline (add runner to CI)
    - Change project properties like visibility
    - Manage CI variables
    - Managing SSH keys
    - Edit profile
- ...

### What works especially good
- Automation of (parameterizable) processes !!!  
- Best GitLab-Plugin IDE I know and working with everything that includes a console !!!

## Example workflow with Lab

```
lab project create gitlab-lab-automation
lab project list
lab clone gitlab-lab-automation
cd gitlab-lab-automation
ll
```
Do you recognize the `.git` directory? You have created an empty repository and cloned it locally.

Now let's create our first issue (to update the issue description you have to redefine title and messages, which is not quite handy):
```
lab issue create -m "Greet the audience" -m "In the audience are sitting cool guys"
lab issue show 1
lab issue edit 1 -m "Greet the audience" -m "In the audience are sitting cool guys..." -m "...and your mother said you should be polite"
lab issue edit 1 -l meetup -a root
lab issue note 1 -m "Please create a precise description. Otherwise nobody knows what to do"
```
The first issue has been created. Let's create a pipeline for that. Update .gitlab-ci.yml
```
greet_the_cool_audience:
  script:
    - echo "Hello Audience"
```
Check the pipeline and push it to the repository:
```
lab ci lint
(invalidate .gitlab-ci.yml by using tab instead of space and validate again to show that linting works)
lab add .
lab commit -m "add greeting pipeline"
lab push
```
Check whether the pipeline succeeds and close the issue:
```
lab ci status
lab ci trace
lab issue close 1
```
Create another issue to create an improved pipeline:
```
lab issue create -m "That's boring, create a cool pipeline" -m "The pipeline should contain different stages and parallel jobs" -l meetup -a root
lab issue list
lab issue show 2
```
Let's create a new branch because we want to use a merge request for the change:
```
lab checkout -b coolpipe
```
Now let's create the improved pipeline. Update .gitlab-ci.yml:
```
stages:
  - synchronous_stage
  - parallel_stage
  - manual_stage

single_rider:
  stage: synchronous_stage
  script: echo "I'm the only one"

parallel_rider1:
  stage: parallel_stage
  script: echo "I'm the first one"

parallel_rider2:
  stage: parallel_stage
  script: echo "No, Im first"

failing_parallel_rider:
  stage: parallel_stage
  allow_failure: true
  script:
    - echo "I'm a loser"
    - exit 1

manual_rider:
  stage: manual_stage
  script: echo "run me manually"
  when: manual
```
Check the yml-file, push it to the repository and check the pipeline progress:
```
lab ci lint
lab commit -am "switch to a cool pipeline"
lab push --set-upstream origin coolpipe
lab ci status
lab ci view (t, T, q, c, r)
```
Now let's create a merge request for the change and vote for it.
```
lab mr create -m "switch to a cool pipeline" -d -l meetup
lab mr list
lab mr show 1
lab mr thumb up 1
```
Finally let's merge the pipeline and check it's progress on the master:
```
lab checkout master
lab mr merge 1
lab ci view
lab issue close 2
```
Though, the -d flag for deleting the branch has been set, the Source branch has to be deleted manually - probably this is a current bug of Lab

## Screenshots

VS-Code
![Lab in VS Code](ci-view-in-vs-code.png "Lab in VS Code")

IntelliJ IDEA
![Lab in IntelliJ](ci-view-in-intelliJ.png "Lab in IntelliJ")

## Setting up the local environment with docker

### configuration from the docs

`sudo docker run --detach --hostname gitlab.example.com --publish 443:443 --publish 80:80 --publish 22:22 --name gitlab --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce:latest`

### Simple configuration when used without gitlab runner

`sudo docker run --detach --hostname gitlab-meetup.localhost --publish 443:443 --publish 80:80 --publish 22:22 --name gitlab --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce:latest`

### Simple configuration when used in combination with docker gitlab-runner

create a docker network (or use an existing one in case you already have one)

`docker network create --subnet=172.18.0.0/24 gitlab`

`sudo docker run --detach --hostname gitlab-meetup.localhost --net gitlab --ip 172.18.0.2 --publish 443:443 --publish 80:80 --publish 22:22 --name gitlab --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce:latest`

`docker run -d --name gitlab-runner --net gitlab --ip 172.18.0.3 --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest`

configure the runner to connect to the local gitlab instance

`docker exec -it gitlab-runner /bin/bash`

`gitlab-runner register`

setup as many runners as you like but give them unique ip addresses, container-names, runner names and volume mounts to unique configuration files

adapt your /etc/hosts of your docker host and all of the runners to publish the hostname of GitLab - it may also be advisable to create a volume mount, in order to persist this information.
add the following entry (to enable lab to connect to the hostname):
`172.18.0.2	gitlab-meetup.localhost`

Register all your runners as a shared runner
- https://docs.gitlab.com/ee/ci/runners/#shared-runners
- https://docs.gitlab.com/runner/register/

When asked which runner type you want you can choose the `shell` runner