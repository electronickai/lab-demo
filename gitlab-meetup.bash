function generate-audience {
cat > .gitlab-ci.yml << EOL
greet_the_cool_audience:
  script:
    - echo "Hello Audience"
EOL
}

function generate-coolpipe {
cat > .gitlab-ci.yml << EOL
stages:
  - synchronous_stage
  - parallel_stage
  - manual_stage

single_rider:
  stage: synchronous_stage
  script: echo "I'm the only one"

parallel_rider1:
  stage: parallel_stage
  script: echo "I'm the first one"

parallel_rider2:
  stage: parallel_stage
  script: echo "No, Im first"

failing_parallel_rider:
  stage: parallel_stage
  allow_failure: true
  script:
    - echo "I'm a loser"
    - exit 1

manual_rider:
  stage: manual_stage
  script: echo "run me manually"
  when: manual
EOL
}

function provision_project {
  lab project create $1
  lab clone $1
  cd $1
  lab issue create -m "Greet the audience" -m "In the audience are sitting cool guys..." -m "...and your mother said you should be polite" -l meetup -a root
  echo "empty" > dummy-file
  lab add .
  lab commit -m "create a dummy file containing the string empty"
  lab push
  lab issue create -m "That's boring, create a cool pipeline" -m "The pipeline should contain different stages and parallel jobs" -l meetup -a root
  lab checkout -b coolpipe
  echo "adapted" > dummy-file
  generate-coolpipe
  git add .
  lab commit -m "adapt the dummy file and add a pipeline"
  lab push --set-upstream origin coolpipe
  lab mr create -m "adapt the dummy file" -d -l meetup -a root
  lab mr thumb up 1
  lab mr merge 1
}

